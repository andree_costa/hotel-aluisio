Rails.application.routes.draw do

  get 'rel_booking/rel_book'

  resources :details
  resources :services


  get 'rel_customer/rel_cust'

  get 'rel_booking/rel_book'

  get 'rel_booking/rel_book0'

  get 'rel_booking/rel_book2'

  get 'rel_booking/rel_book3'



  resources :orders
  devise_for :admins
  devise_for :members
  get 'home/index'

  get 'inicio' => 'home#index'

  root 'home#index'

  get 'relatorio' => 'rel_customer#rel_cust'

  resources :items
  resources :companions
  resources :bookings
  resources :cars
  resources :customers
  resources :situations
  resources :reasons
  resources :products
  resources :federations
  resources :employees
  resources :graduations
  resources :forces
  resources :apartments



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
