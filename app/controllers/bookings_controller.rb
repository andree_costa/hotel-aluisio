class BookingsController < ApplicationController
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

      # filtro devise 
   # before_action :authenticate_member!
   # before_action :authenticate_admin!


  # GET /bookings
  # GET /bookings.json
  def index
    @q2= Booking.ransack(params[:q2].try(:merge, m: 'or' ))
    @bookings = @q2.result

    @q= Booking.ransack(params[:q].try(:merge, m: params[:combinator]))
    @bookings = @q.result
   # @bookings = Booking.all
  end

  
  # GET /bookings/1
  # GET /bookings/1.json
  def show
    @parametros = params
    @varBooking_id = Booking.find(params[:id])
    #@var_id = @varBooking_id.id
   # @var_id2 = params[:id]

  end

  # GET /bookings/new
  def new
    @booking = Booking.new
    @booking.build_customer
    @clientes = Customer.all
    @apartamentos = Apartment.all
    @graduacoes = Graduation.all
    @empregados = Employee.all
    @motivos = Reason.all
    @pedidos = Order.all

  end

  # GET /bookings/1/edit
  def edit
    @clientes = Customer.all
    @apartamentos = Apartment.all
    @graduacoes = Graduation.all
    @empregados = Employee.all
    @motivos = Reason.all
  end

  # POST /bookings
  # POST /bookings.json
  def create
    @booking = Booking.new(booking_params)

    respond_to do |format|
      if @booking.save
        format.html { redirect_to @booking, notice: 'Booking was successfully created.' }
        format.json { render :show, status: :created, location: @booking }
      else
        format.html { render :new }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  def update
    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to @booking, notice: 'Booking was successfully updated.' }
        format.json { render :show, status: :ok, location: @booking }
      else
        format.html { render :edit }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to bookings_url, notice: 'Booking was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.require(:booking).permit(:solicitante, :celular_soli, :email_soli, :cidade_soli, :uf_soli,
         :om_orgao_cli, :dt_reserva, :dt_entrada, :dt_saida, :dt_pgto, :add_produto, :add_apto, :desconto,
          :status_res, :obs, :customer_id, :apartment_id, :graduation_id, :employee_id, :reason_id, :order_id, :force_id, :situation_id,
           :order_attributes => [:nome_acompanhante], :situations_attributes => [:id, :situacao],
            :forces_attributes => [:id, :forca], :customer_attributes => [:id, :nome, :identidade,
             :cpf, :celular, :fone_res, :dt_cadastro, :email, :cidade, :federation],
              :companions_attributes => [:id, :idt, :nome_acompanhante, :parentesco, :idade, :sexo, :booking_id, :_destroy])
    end
end
