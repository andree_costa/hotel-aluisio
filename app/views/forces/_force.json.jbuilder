json.extract! force, :id, :forca, :created_at, :updated_at
json.url force_url(force, format: :json)
