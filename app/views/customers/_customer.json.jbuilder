json.extract! customer, :id, :nome, :identidade, :cpf, :celular, :fone_res, :dt_cadastro, :email, :cidade, :federation, :force_id, :situation_id, :created_at, :updated_at
json.url customer_url(customer, format: :json)
