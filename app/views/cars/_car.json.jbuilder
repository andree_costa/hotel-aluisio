json.extract! car, :id, :marca_mod, :cor, :placa, :customer_id, :created_at, :updated_at
json.url car_url(car, format: :json)
