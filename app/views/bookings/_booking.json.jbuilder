json.extract! booking, :id, :solicitante, :celular_soli, :email_soli, :cidade_soli, :uf_soli, :om_orgao_cli, :dt_reserva, :dt_entrada, :dt_saida, :dt_pgto, :add_produto, :add_apto, :desconto, :status_res, :obs, :customer_id, :apartment_id, :graduation_id, :employee_id, :reason_id, :created_at, :updated_at
json.url booking_url(booking, format: :json)
