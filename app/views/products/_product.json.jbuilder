json.extract! product, :id, :produto, :qtde_estq, :estq_min, :unidade, :dt_entrada, :dt_vencimento, :valor, :created_at, :updated_at
json.url product_url(product, format: :json)
