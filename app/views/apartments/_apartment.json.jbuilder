json.extract! apartment, :id, :nr, :descricao, :leito, :status, :observacao, :created_at, :updated_at
json.url apartment_url(apartment, format: :json)
