json.extract! companion, :id, :idt, :nome_acompanhante, :parentesco, :idade, :sexo, :booking_id, :created_at, :updated_at
json.url companion_url(companion, format: :json)
