json.extract! graduation, :id, :posto_grad, :descricao, :created_at, :updated_at
json.url graduation_url(graduation, format: :json)
