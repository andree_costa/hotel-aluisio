json.extract! federation, :id, :uf, :estado, :created_at, :updated_at
json.url federation_url(federation, format: :json)
