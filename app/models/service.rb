class Service < ApplicationRecord
	has_many :details
	##has_many :products, through: :items 

	accepts_nested_attributes_for :details, reject_if: :all_blank, allow_destroy: true

end
 