class Customer < ApplicationRecord
  #belongs_to :force
  #belongs_to :situation

  belongs_to :booking

  validates :dt_cadastro, presence: true

  has_many :cars
  accepts_nested_attributes_for :cars, reject_if: :all_blank, allow_destroy: true
  
end
