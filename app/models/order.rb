class Order < ApplicationRecord
	#belongs_to :booking
	has_many :items
	has_many :products, through: :items 

	
	accepts_nested_attributes_for :items, reject_if: :all_blank, allow_destroy: true

end
