class Booking < ApplicationRecord
  belongs_to :apartment
  belongs_to :graduation
  belongs_to :employee
  belongs_to :reason
  belongs_to :order

  belongs_to :force
  belongs_to :situation


  
  has_many :companions

  validates :dt_entrada, presence: true
  validates :dt_saida, presence: true
  validates :dt_reserva, presence: true
  validates :dt_pgto, presence: true
 

  has_one :customer
  accepts_nested_attributes_for :customer
  accepts_nested_attributes_for :force
  accepts_nested_attributes_for :situation 
  accepts_nested_attributes_for :companions, reject_if: :all_blank, allow_destroy: true
end
