class Item < ApplicationRecord
  belongs_to :order
  belongs_to :product
  #has_many :product


  accepts_nested_attributes_for :product, reject_if: :all_blank, allow_destroy: true
end
