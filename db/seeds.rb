# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


#Situation
Situation.create({situacao: 'Ativa'})
Situation.create({situacao: 'Reserva'})
Situation.create({situacao: 'Nv Superior'})
Situation.create({situacao: 'Nv Intermediário'})
Situation.create({situacao: 'Nv Médio'})

# Força

Force.create({forca: 'EB'})
Force.create({forca: 'FAB'})
Force.create({forca: 'MB'})
Force.create({forca: 'F Aux'})
Force.create({forca: 'Sv Civil'})

# Razão

Reason.create({motivo: 'A serviço'})
Reason.create({motivo: 'Trânsito'})
Reason.create({motivo: 'Arejamento'})
Reason.create({motivo: 'Férias'})
Reason.create({motivo: 'Trat saúde'})
Reason.create({motivo: 'Lazer'})
Reason.create({motivo: 'Outros'})

# Clientes


# Apartamentos

Apartment.create({nr: '01', descricao: 'Suite General', leito: ' 1 cama casal 1 solteiro', status: '1LV', observacao:'perfeito estado'})
Apartment.create({nr: '02', descricao: 'Suíte Master', leito: ' 1 cama casal ', status: '1LVR', observacao:'Ok.'})
Apartment.create({nr: '03', descricao: 'Quarto amplo ', leito: '2 camas ', status: '2OCP', observacao:'em Manutenção'})
Apartment.create({nr: '04', descricao: 'Quarto simples', leito: '2 camas solteiro', status: '1LVR', observacao:'OK.'})
Apartment.create({nr: '05', descricao: 'Quarto simples', leito: '2 camas solteiro', status: '2OCP', observacao:'OK.'})
Apartment.create({nr: '06', descricao: 'Quarto Master', leito: '2 camas casal', status: '1LVR', observacao:'OK.'})
Apartment.create({nr: '07', descricao: 'Suíte Master', leito: '2 camas casal', status: '2OCP', observacao:'OK.'})
Apartment.create({nr: '08', descricao: 'Suite General', leito: '2 camas solteiro ', status: '1LVR', observacao:'OK.'})
Apartment.create({nr: '09', descricao: 'Quarto simples', leito: '2 camas solteiro', status: '1LVR', observacao:'OK.'})
Apartment.create({nr: '10', descricao: 'Quarto simples', leito: '2 camas solteiro', status: '2OCP', observacao:'OK.'})


#Funcionarios
Employee.create({empregado: 'Aluisio', celular: '(83) 98662-0331', funcao: 'Admin', login: 'alfit@oi.com.br', senha: '123456'})
Employee.create({empregado: 'Romildo', celular: '(83) 98555-0123', funcao: 'Admin', login: 'romildo@gmail', senha: '123456'})
Employee.create({empregado: 'Sena', celular: '(83) 98662-9876', funcao: 'Membro', login: 'sena@gmail.com', senha: '123456'})


# Graduação

Graduation.create({posto_grad: 'Gen Ex', descricao: 'General de Exército'})
Graduation.create({posto_grad: 'Gen Div', descricao: 'General de Divisão'})
Graduation.create({posto_grad: 'Gen Bda', descricao: 'General de Brigada'})
Graduation.create({posto_grad: 'Cel', descricao: 'Coronel'})
Graduation.create({posto_grad: 'Ten Cel', descricao: 'Tenente Coronel'})
Graduation.create({posto_grad: 'Maj', descricao: 'Major'})
Graduation.create({posto_grad: 'Cap', descricao: 'Capitão'})
Graduation.create({posto_grad: '1º Ten', descricao: '1º Tenente'})
Graduation.create({posto_grad: '2º Ten', descricao: '2º Tenente'})
Graduation.create({posto_grad: 'Asp', descricao: 'Aspirante'})
Graduation.create({posto_grad: 'Cad', descricao: 'Cadete'})


