class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.integer :nr_res
      t.string :nome
      t.date :dt_pedido

      t.timestamps
    end
  end
end
