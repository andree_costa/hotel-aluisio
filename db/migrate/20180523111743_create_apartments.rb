class CreateApartments < ActiveRecord::Migration[5.1]
  def change
    create_table :apartments do |t|
      t.integer :nr
      t.string :descricao
      t.string :leito
      t.string :status
      t.string :observacao

      t.timestamps
    end
  end
end
