class CreateCustomers < ActiveRecord::Migration[5.1]
  def change
    create_table :customers do |t|
      t.string :nome
      t.string :identidade
      t.string :cpf
      t.string :celular
      t.string :fone_res
      t.date :dt_cadastro
      t.string :email
      t.string :cidade
      t.string :federation
      t.references :force, foreign_key: true
      t.references :situation, foreign_key: true

      t.timestamps
    end
  end
end
