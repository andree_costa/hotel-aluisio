class CreateDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :details do |t|
      t.references :service, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :qtde
      t.decimal :valor

      t.timestamps
    end
  end
end
