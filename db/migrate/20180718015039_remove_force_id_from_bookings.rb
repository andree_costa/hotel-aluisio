class RemoveForceIdFromBookings < ActiveRecord::Migration[5.1]
  def change
    remove_column :bookings, :force_id, :integer
  end
end
