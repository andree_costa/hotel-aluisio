class RemoveSituationIdFromCustomers < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :customers, column: :situation_id
  end
end
