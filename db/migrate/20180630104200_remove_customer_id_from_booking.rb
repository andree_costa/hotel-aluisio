class RemoveCustomerIdFromBooking < ActiveRecord::Migration[5.1]
  def change
  	remove_foreign_key :bookings, column: :customer_id
    
  end
end
