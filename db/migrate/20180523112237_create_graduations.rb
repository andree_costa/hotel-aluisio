class CreateGraduations < ActiveRecord::Migration[5.1]
  def change
    create_table :graduations do |t|
      t.string :posto_grad
      t.string :descricao

      t.timestamps
    end
  end
end
