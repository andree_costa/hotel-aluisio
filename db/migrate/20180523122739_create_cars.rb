class CreateCars < ActiveRecord::Migration[5.1]
  def change
    create_table :cars do |t|
      t.string :marca_mod
      t.string :cor
      t.string :placa
      t.references :customer, foreign_key: true

      t.timestamps
    end
  end
end
