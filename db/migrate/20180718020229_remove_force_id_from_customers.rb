class RemoveForceIdFromCustomers < ActiveRecord::Migration[5.1]
  def change
    remove_foreign_key :customers, column: :force_id
  end
end
