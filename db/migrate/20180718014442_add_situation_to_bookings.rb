class AddSituationToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :situation_id, :integer
  end
end
