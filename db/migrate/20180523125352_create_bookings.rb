class CreateBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings do |t|
      t.string :solicitante
      t.string :celular_soli
      t.string :email_soli
      t.string :cidade_soli
      t.string :uf_soli
      t.string :om_orgao_cli
      t.date :dt_reserva
      t.date :dt_entrada
      t.date :dt_saida
      t.date :dt_pgto
      t.float :add_produto
      t.float :add_apto
      t.float :desconto
      t.string :status_res
      t.string :obs
      t.references :customer, foreign_key: true
      t.references :apartment, foreign_key: true
      t.references :graduation, foreign_key: true
      t.references :employee, foreign_key: true
      t.references :reason, foreign_key: true

      t.timestamps
    end
  end
end
