class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :produto
      t.integer :qtde_estq
      t.integer :estq_min
      t.string :unidade
      t.date :dt_entrada
      t.date :dt_vencimento
      t.float :valor

      t.timestamps
    end
  end
end
