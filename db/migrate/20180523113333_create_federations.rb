class CreateFederations < ActiveRecord::Migration[5.1]
  def change
    create_table :federations do |t|
      t.string :uf
      t.string :estado

      t.timestamps
    end
  end
end
