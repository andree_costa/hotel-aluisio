class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :empregado
      t.string :celular
      t.string :funcao
      t.string :login
      t.string :senha

      t.timestamps
    end
  end
end
