class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.date :dt_service
      t.string :pessoa
      t.integer :nr_res

      t.timestamps
    end
  end
end
