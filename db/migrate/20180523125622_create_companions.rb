class CreateCompanions < ActiveRecord::Migration[5.1]
  def change
    create_table :companions do |t|
      t.string :idt
      t.string :nome_acompanhante
      t.string :parentesco
      t.integer :idade
      t.string :sexo
      t.references :booking, foreign_key: true

      t.timestamps
    end
  end
end
