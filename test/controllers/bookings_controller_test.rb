require 'test_helper'

class BookingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @booking = bookings(:one)
  end

  test "should get index" do
    get bookings_url
    assert_response :success
  end

  test "should get new" do
    get new_booking_url
    assert_response :success
  end

  test "should create booking" do
    assert_difference('Booking.count') do
      post bookings_url, params: { booking: { add_apto: @booking.add_apto, add_produto: @booking.add_produto, apartment_id: @booking.apartment_id, celular_soli: @booking.celular_soli, cidade_soli: @booking.cidade_soli, customer_id: @booking.customer_id, desconto: @booking.desconto, dt_entrada: @booking.dt_entrada, dt_pgto: @booking.dt_pgto, dt_reserva: @booking.dt_reserva, dt_saida: @booking.dt_saida, email_soli: @booking.email_soli, employee_id: @booking.employee_id, graduation_id: @booking.graduation_id, obs: @booking.obs, om_orgao_cli: @booking.om_orgao_cli, reason_id: @booking.reason_id, solicitante: @booking.solicitante, status_res: @booking.status_res, uf_soli: @booking.uf_soli } }
    end

    assert_redirected_to booking_url(Booking.last)
  end

  test "should show booking" do
    get booking_url(@booking)
    assert_response :success
  end

  test "should get edit" do
    get edit_booking_url(@booking)
    assert_response :success
  end

  test "should update booking" do
    patch booking_url(@booking), params: { booking: { add_apto: @booking.add_apto, add_produto: @booking.add_produto, apartment_id: @booking.apartment_id, celular_soli: @booking.celular_soli, cidade_soli: @booking.cidade_soli, customer_id: @booking.customer_id, desconto: @booking.desconto, dt_entrada: @booking.dt_entrada, dt_pgto: @booking.dt_pgto, dt_reserva: @booking.dt_reserva, dt_saida: @booking.dt_saida, email_soli: @booking.email_soli, employee_id: @booking.employee_id, graduation_id: @booking.graduation_id, obs: @booking.obs, om_orgao_cli: @booking.om_orgao_cli, reason_id: @booking.reason_id, solicitante: @booking.solicitante, status_res: @booking.status_res, uf_soli: @booking.uf_soli } }
    assert_redirected_to booking_url(@booking)
  end

  test "should destroy booking" do
    assert_difference('Booking.count', -1) do
      delete booking_url(@booking)
    end

    assert_redirected_to bookings_url
  end
end
